
import CredentialsProvider from "next-auth/providers/credentials";
import NextAuth from "next-auth";

function Login(credentials) {
  const userName = "Admin"
  const password = "123456"
  return new Promise((resolve, reject) => {
    if(userName===credentials.username){

      if(password===credentials.password){
        resolve("good")
      }
      else reject("not good")
    }
    else reject("not good") 
  });
}

export const authOptions = {
  providers: [
    CredentialsProvider({
      name: "LDAP",
      credentials: {
        username: { label: "Username", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials) {
        if (
          credentials
        ) {
          try {
            await Login(credentials);
            const user = {userId: "1"}
            console.log(user);
            if (!user.userId) {
              return null;
            }

            return user;
          } catch {
            return null;
          }
        }

        return null;
      },
    }),
  ],
  callbacks: {
    jwt({ token, user }) {
      return user ? { ...token, user } : token;
    },
    session({ session, token }) {
      return token.user ? { ...session, user: token.user } : session;
    },
  },
};

export default NextAuth(authOptions);
