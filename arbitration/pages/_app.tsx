import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { SessionProvider, useSession } from "next-auth/react";
import Head from "next/head";
import Login from "../components/auth/login";
import Loading from "../components/loading";
import Layout from "../components/layout";

function AuthRequired({ children, LoginComponent }: any) {
  const { status } = useSession();

  return status === "authenticated" ? (
    children
  ) : status === "loading" ? (
    <Loading size="60" />
  ) : (
    <LoginComponent />
  );
}

export default function App( props : AppProps) {
  const {
    Component,
    pageProps: { session, ...pageProps },
    ...appProps
  } = props;
  console.log(pageProps.session, "dfsdfsdfsdfsa");

  
  return  <>
      <Head>
          <title>Arbitration</title>
          <link href="/favicon.svg" rel="shortcut icon" />
      </Head>
      <SessionProvider refetchOnWindowFocus={false} session={session}>
        <AuthRequired LoginComponent={Login}>
          <Layout>
              <Component {...pageProps} />
          </Layout>
        </AuthRequired>
      </SessionProvider>
    </>
    
  // <Component {...pageProps} />
}
