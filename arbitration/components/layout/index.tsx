import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Logout from "../auth/logout";

export default function Layout({ children }:any) {
  return (
    <Box
      sx={{
        display: "flex",
        maxWidth: "100%",
      }}
    >
      <AppBar 
      sx = {{height:50,}}
    >
      <Box
      sx={{
        display: "flex",
        justifyContent: "right",
        mb: 2,
        mr: 1,
      }}
      >
        <Logout/>
      </Box>
    </AppBar>
      <Box
        sx={{
          width: "100%",
          height:"100vh",
          backgroundColor:"white",
          p:10
        }}
      >
        {children}
      </Box>
    </Box>
  );
}
