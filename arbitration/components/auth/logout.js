import * as React from "react";
import Button from "@mui/material/Button";
import LogoutIcon from "@mui/icons-material/Logout";
import { signOut } from "next-auth/react";

export default function Logout() {

  
  return<Button
          variant="outlined"
          sx = {{color: "white"}}
          onClick={() => signOut({ redirect: false })}
          startIcon={<LogoutIcon />}
        >
          Гарах
      </Button>
}
